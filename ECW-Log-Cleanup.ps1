$logpath = 'C:\temp'
$logname = 'ECW-log-Truncate'
$FileLogdate = Get-Date -format 'MMddyyyy'
$mobiledoc = 'D:\eClinicalWorks\tomcat8_43855\webapps\mobiledoc\logs'
$tomcat = 'D:\eClinicalWorks\tomcat8_43855\logs'
$temphubpics = 'D:\eClinicalWorks\tomcat8_43855\webapps\mobiledoc\temphubpics'
$days = (Get-Date).AddDays(-7)
$days2 = (Get-Date).AddDays(-1)
$ErrorView = 'CategoryView'
$ErrorActionPreference = "SilentlyContinue"
$service = 'Tomcat8_43855_06072'

Function Admin-Permission {
    $currentPrincipal = New-Object Security.Principal.WindowsPrincipal( [Security.Principal.WindowsIdentity]::GetCurrent() )
    if ($currentPrincipal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator )) {
        Write-Verbose "script Running as Admin" -Verbose
        return $true
    } else {
    return $false
    }
}

if (-not (Admin-Permission)) {
	Write-Verbose $msgNewLine -Verbose
	Write-Warning "This script needs to be executed as an Administrator"
	$Error.Clear()
	Start-Sleep -Seconds 2
	exit
}

if (!(Test-Path $logpath -PathType Container)) {
    New-Item -ItemType Directory -Force -Path $logpath
    Write-Verbose "$logpath folder succesfully created" -Verbose
}

New-Item -Path $logpath -Name "$logname-$FileLogdate.log" -ItemType "file"
Start-Transcript -Path "$logpath\$logname-$FileLogdate.log" -Append
Get-Childitem $logpath\$logname* -Recurse | where {$_.LastwriteTime -lt (Get-Date).AddDays(-3)} | Remove-Item –Force -Verbose


$Delete = Get-Childitem $mobiledoc -Recurse | where {$_.LastwriteTime -le "$days"}
foreach ($File in $Delete)
{
    if ($File -ne $Null)
    {
        Write-Verbose "Deleting File $File" -Verbose
        Remove-item -Recurse -force $File.Fullname | out-null
    }
    else {
        Write-Verbose "No more files to delete" -Verbose
    }
     }
$Delete = Get-Childitem $tomcat -Recurse | where {$_.LastwriteTime -le "$days"}

foreach ($File in $Delete)
{
    if ($File -ne $Null)
    {
        Write-Verbose "Deleting File $File" -Verbose
        Remove-item -Recurse -force $File.Fullname | out-null
    }
    else {
        Write-Verbose "No more files to delete" -Verbose
    }
     }
$Delete = Get-Childitem $temphubpics -Recurse | where {$_.LastwriteTime -le "$days2"}

foreach ($File in $Delete)
{
    if ($File -ne $Null)
    {
        Write-Verbose "Deleting File $File" -Verbose
        Remove-item -Recurse -force $File.Fullname | out-null
    }
    else {
        Write-Verbose "No more files to delete" -Verbose
    }
     }
# Stop Tomcat service 
Write-Host "Stopping Tomcat service..."
Stop-Service -Name $service

# Delete Catalina work directory 
Write-Host "Deleting Catalina work directory..." 
Remove-Item -Path 'D:\eClinicalWorks\tomcat8_43855_06072\work\Catalina' -Recurse -Force 

# Start Tomcat service 
Write-Host "Starting Tomcat service..." 
Start-Service -Name $service

Start-Sleep -s 90

if ($service.Status -eq "Running") {
    Write-Host "$service Running"
} else {
    Write-Host "$service Not Running"
}
     $Error
  stop-transcript 
